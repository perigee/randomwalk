+++
title =  "Gitlab troubleshooting"
date = 2019-07-17T21:26:24Z
tags = ['gitlab']
featured_image = ""
description = ""
+++

### Issues


#### Gitlab UI 500 issue

As gitlab on-premise admin, recently experience many operation issues with the tool. One issue frequently resurfacing is certain user expericens the 500 http code on gitlab UI. One of the bug causes such issue is:

{{< imgRel 
pathURL="img/500.png" 
alt="500 error" >}}

```
ActionView::Template::Error (No route matches {:action=>"show", :controller=>"admin/projects", :id=>#<Project id:6946 services/teams/devops/services/testmetaservicename/web-services/sygentestunit2/sygentestunit2-dev>, :namespace_id=>nil}, possible unmatched constraints: [:namespace_id]):
```

such bug can be fixed by logging into `gitlab-rails console`:


```
gitlab-rails console
> Project.find_by_full_path('NAMESPACE/PROJECT').repository.expire_all_method_caches


# Delete corruted project from user
user = User.find_by(email: 'your@mysoft.com')  # will show user_id
to_delete = Project.unscoped.where(pending_delete: true).first
ProjectDestroyWorker.perform_async(to_delete.id, user.id, {})
```


#### Customized service in Gitlab CI


CI defines a `service` keyword for lanunching container as a dependent
resource for testing project, anyone can define his own customized
service, for example, in `.gitlab-ci.yml`:

```
image: perigee/toolbox:2.22.1

stages:
  - show


show:
  stage: show
  services:
    - name: perigee/dind:tmp # from docker:dind
      alias: docker
  script:
    - docker ps

```


the `alias` will be the service name for the testing, `docker` alias
is dedicated for `docker:dind` service. For other services, may refer
`docker-compose` service definition. Need be careful with the `alias`
which will be cut after `:`([ref](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services)). 



### Migration

#### Gitlab postgresql database migration




##### Reference:

- [up, down]http://vaidehijoshi.github.io/blog/2015/05/19/the-secret-life-of-your-database-part-1-migrations/
- [database]https://docs.gitlab.com/omnibus/settings/database.html
- [maintenance]https://docs.gitlab.com/omnibus/maintenance/
- [rake]https://docs.gitlab.com/ee/administration/raketasks/maintenance.html
- [rail console]https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html

### Maintenance

#### User management

[Disable](https://stackoverflow.com/questions/33239048/how-to-prevent-group-creation-in-gitlab) the group creation right on current all users:

In console: `gitlab-rails console`



```
> User.update_all(can_create_group:false)
```


or just user runner (not ci runner)


```
gitlab-rails runner 'User.update_all(can_create_group:false)'
```

please do not run above command in `docker exec` mode, it will crash the entire system.

For future users, add following config in `gitlab.rb`:

```
gitlab_rails['gitlab_default_can_create_group'] = false
```


##### References

- [alias](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)

### References:

- https://docs.gitlab.com/omnibus/maintenance/
- https://gitlab.com/gitlab-org/gitlab-ce/issues/52897
