---
title: "PDF and JPG manipulation under linux"
date: 2020-02-12T16:04:38Z
publishdate: 2020-02-12
lastmod: 2020-02-12
tags: ['pdf','jpg']
---





#### JPG


##### Reduce the size of JPG

```
jpegoptim --size=150k 1.jpg
```

##### Convert JPG to PDF


```
img2pdf *.jpg --output output.pdf
```


#### PDF


##### Combine the PDFs to single PDF


```
pdftk 1.pdf 2.pdf cat output output.pdf
```


##### Compress PDF

`gs` is not very effective

```
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
```

