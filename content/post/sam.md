+++
title = "sam"
date =  2019-09-10T17:51:56Z
tags = ['aws']
+++

#### AWS SAM 101


After installation, workflow of SAM could be:

```
sam init --runtime python3.7 -n YOUR_PROJECT

sam build

sam package --s3-bucket $(S3_BUCKET) \
    --s3-prefix $(S3_PREFIX) \
    --output-template-file packaged.yaml

sam deploy --template-file ./packaged.yaml \
    --stack-name $(STACK_NAME) \
    --capabilities CAPABILITY_IAM
```