---
title: "Collapse by J. Diamond"
date: 2019-08-30T15:34:11Z
publishdate: 2019-08-30
lastmod: 2019-08-30
draft: false
tags: [phi]
---


Jared Diamond's `Collapse` is my best reading recently. His writing
method is so well developed to clearly explain his theories.



### References:

- [Collapse book](https://www.amazon.ca/Collapse-Societies-Choose-Succeed-Revised/dp/0143117009)

