---
title: "Workflow"
date: 2020-07-07T14:39:53Z
publishdate: 2020-07-07
lastmod: 2020-07-07
tags: ['design']
---

### Workflow


#### Not to

##### Do not break the common sense

For example, once the configuration of a process is change, the
process supposely needs to be restart/reload. If such workflow were
interrupted, an human error may happen. In systemd, you can configure
a service waiting for another services to start, it may create such
confusion.


