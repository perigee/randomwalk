FROM registry.gitlab.com/pages/hugo:0.55.6

RUN /usr/sbin/addgroup hugo && /usr/sbin/adduser -S -D -u 1000 hugo
RUN apk add --update --no-cache \
    make


EXPOSE 1313