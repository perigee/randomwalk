.PHONY: all


SERVICE_NAME := hugo
VIRTUAL_HOST="http://docker.local:1313"

export

build:
	docker build -t $(SERVICE_NAME) .

local: build
	docker run -ti --rm -v $(PWD):/home/hugo \
	-w="/home/hugo" \
	-p 1313:1313 \
	--rm \
	-u hugo \
	$(SERVICE_NAME) /bin/sh

new:
	hugo new post/$(POST).md

dev:
	hugo --renderToDisk=true --watch=true --bind="0.0.0.0" \
	server -D
